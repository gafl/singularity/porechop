# porechop Singularity container
Bionformatics package porechop<br>
Adapter removal and demultiplexing of Oxford Nanopore reads
https://github.com/rrwick/Porechop
porechop Version: 0.2.3<br>

Singularity container based on the recipe: Singularity.porechop_v0.2.3

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull porechop_v0.2.3.sif oras://registry.forgemia.inra.fr/gafl/singularity/porechop/porechop:latest


